//bootswatch
//DOM
//Document Object Model
//Document----------
// Dom manipulation means, we can access any part of our DOM using Javascript.
//
//How do we access specific parts of our DOM?
//
//
//console.log(document);
//
//Access an element via ID

// const h1 = document.getElementById('mainHeader');

// console.log(h1);

//to change TEXT CONTENT
// h1.textContent = "Bye World!";

//to change text content AND html
// h1.innerHTML = `Bye <span class="text-danger">World!</span>`

// to CHANGE STYLE
//
// h1.style.color = 'salmon';
// h1.style.fontSize = '50px';
//In Javascript, css properties are written in camel case 'camelCase'.

//to MODIFY class,
//to modify class, remember that we need to access first the classlist of the element.

//to add a class
// h1.classList.add('text-danger');
// h1.classList.add('text-danger');

//to remove a class

// h1.classList.remove('text-danger');

//get elements via their class names
// const lis = document.getElementsByClassName('color');
// console.log(lis);
//this will return an HTM Collection
//class is to re-use
//ID is for the unique property

//An HTML Collection is an array-like type of data.
//This represents the elements.
//And we can only access it via index.

// sample lis[0].textContent = "Not Red"; 

// Although it is array-like we cant use array loop methods.
//lis.forEach(function(indivLi){console.log(indivLi);});
// x forEach, map, filter---->error

//for(let index=0; index < lis.length; index++){console.log(lis[index]);} = this is ONLY WORKING for looping what inside the class

//YES for loop 

//get elements via their tags
//this will also returen an html collection
// const uls = document.getElementsByTagName('ul')
// console.log(uls);

//QuerySelector
//returns nodelist instead of HTML collection
//this includes all types of nodes like elements, text nodes.

//query selector accepts css selector as a parameter
//query selector will return the first element that will satisfy the parameter
// const h1 = document.querySelector('#mainHeader');
// console.log(h1);

//
// const li = document.querySelector('.color');
// console.log('li');

//querySelectorAll
//it will return all elements that will satisfy the parameter

const lis = document.querySelectorAll('.color');
console.log('lis');
//A node list is also an array-like type of data. 
//We can access individual data via its index.

//Unlike HTML collection, we can use array loop methods in node list.
//Nodelist can loop = YES
//lis.forEach(function(indivLi){console.log(indivLi)}); ---WORKING /YES

// activity:
//tbody;
//body.firstElementChild.lastElementChild.lastElementChild.lastElementChild.firstElementChild.nextElementSibling;
//<td>​Baguio​</td>​

// NOTES SIR
// ----------------------
//saved in NotePad: June 23, 2020.
//Quiz:
//From h1 with ID Main-Title
//
//Target the following elements:
//1. Body
// 2. tHead
// 3. td - Palawan
// 4. th - spot
// 5. td -2
// 6. td - Aklan
// 7. td - Benguet
// Activity Name: domtraversalactivty.js 
// Be back at 8pm


