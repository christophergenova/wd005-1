//dontraversal
//FIRST RULE:
//CODE PROPERLY. IDENT PROPERLY!
// Traversal from top to ---> down
//
//Task target the body via id.
//solution: we can use either document.getElementById or
//document.querySelector

const tbody = document.getElementById('listBody');

//const tbody = document.querySelector('#listBody');
// querySelector uses how class/id call in CSS

//to access first child
//parentElement.firstElementChild;
//tbody.firstElementChild; (ACTUAL CODE)
//
//to access last child
//tbody.lastElementChild;
//
//traversal bottom----> up;
//to access parent
//tbody.parentElement;
//
//traversal same-parent
//tbody.previousElementSibling;

const thead = document.getElementById('listHead');

//below thead
//thead.nextElementSibling;

//Quiz:
//From h1 with ID Main-Title
//
//Target the following elements:
// 1. Body
// 2. tHead
// 3. td - Palawan
// 4. th - spot
// 5. td -2
// 6. td - Aklan
// 7. td - Benguet
// Activity Name: domtraversalactivty.js 
// Be back at 8pm

// submit here: wd005-1 DOM Manipulation 1: Selecting Elements
const h1 = document.getElementById('main-title');






// 1. h1.parentElement.parentElement.parentElement; ----> Target: Body



// 2. h1.parentElement.nextElementSibling.firstElementChild; ----> Target: thead

// 3. h1.parentElement.nextElementSibling.lastElementChild.firstElementChild.lastElementChild;  ----> Target: TD Palawan

// 4. h1.parentElement.nextElementSibling.firstElementChild.firstElementChild.firstElementChild.nextElementSibling; ----> Target: TH Spot

// 5. h1.parentElement.nextElementSibling.lastElementChild.firstElementChild.nextElementSibling.firstElementChild; ----> Target: TD 2

// 6. h1.parentElement.nextElementSibling.lastElementChild.firstElementChild.nextElementSibling.lastElementChild; ----> Target: TD Aklan

// 7. h1.parentElement.nextElementSibling.lastElementChild.lastElementChild.lastElementChild; ----> Target: TD Benguet